"use strict";

body.className = localStorage.getItem("stateMode");

function toggleDarkLight() {
const body = document.getElementById("body");
const currentClass = body.className;
body.className = currentClass == "dark-mode" ? "light-mode" : "dark-mode";
localStorage.setItem("stateMode", body.className);
}