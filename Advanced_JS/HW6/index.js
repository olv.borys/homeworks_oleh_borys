"use strict";

async function getAddress() {
  let div = document.createElement("div");
  div.classList.add("div");
  let btn = document.getElementById("btn");
  div.innerHTML = `Шукаємо Ваше місце перебування...`;
  btn.after(div);

  let ipResponse = await fetch("https://api.ipify.org/?format=json");
  let jsonIpResp = await ipResponse.json();
  let ip = jsonIpResp.ip;

  let addressResponse = await fetch(`http://ip-api.com/json/${ip}`);
  let addressJson = await addressResponse.json();
  let { timezone, country, regionName, city, zip } = addressJson;
  setTimeout(() => {
    div.innerHTML = `<strong>Ваше місцезнаходження:</strong>  ${timezone},  ${country},  ${regionName},  ${city},  ${zip}`;
  }, 2000);
}
