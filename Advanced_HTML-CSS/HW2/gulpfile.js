const { parallel, series } = require("gulp");
const { scripts } = require("./gulp-tasks/scripts.js");
const { styles } = require("./gulp-tasks/styles.js");
const { images } = require("./gulp-tasks/images.js");
const { serv } = require("./gulp-tasks/serv.js");
const { watcher } = require("./gulp-tasks/watcher.js");
const { cleaner } = require('./gulp-tasks/cleaner.js');

exports.build = series(cleaner, parallel(images, styles, scripts),serv, watcher);
exports.dev = parallel(serv, watcher, series( styles, scripts));

