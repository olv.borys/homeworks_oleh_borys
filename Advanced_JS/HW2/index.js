"use strict";

const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
     
    }
  ];

  let root = document.getElementById('root');
  let ul = document.createElement('ul');
  root.appendChild(ul);



books.forEach (el => {
  try {
      if (!el.price || !el.name || !el.author) {
        const err = (!el.price ? 'price' : ' ' ) + (!el.name ? 'name' : ' ') + (!el.author ? 'autor' : ' ')
        throw new SyntaxError(`Дані невірні ${err}`);
      }

      let li = document.createElement('li');
      li.innerHTML = el.author + ' ' + el.name + ' ' + el.price;
      ul.appendChild(li);
  }
  catch(e){
    console.log(e,'error')
  }
})



    

   