"use strict";

const FILMS_URL = "https://ajax.test-danit.com/api/swapi/films";

class Poster {
  constructor(films_url) {
    this.FILMS_URL = films_url;
    this.film = document.querySelector(".film");
  }

  render() {
    this.doGetFetchRequest(this.FILMS_URL)
      .then((response) => {
        this.films = response;
        this.films = this.films.sort((a, b) => a.episodeId - b.episodeId);
        this.renderFilms();
      })
      .then(() => {
        this.renderCharacters();
      });
  }

  renderFilms() {
    this.ulFilms = document.createElement("ul");
    this.ulFilms.classList.add("films");
    this.ulFilmName = document.createElement("li");
    this.ulFilmName.classList.add("films__name");
    this.ulEposode = document.createElement("ul");
    this.ulEposode.classList.add("films__episode");
    this.ulTitle = document.createElement("ul");
    this.ulTitle.classList.add("films__title");
    this.ulOpeningCrawl = document.createElement("ul");
    this.ulOpeningCrawl.classList.add("films__opening-crawl");
    this.createFilms();
  }

  createFilms() {
    for (const film of this.films) {
      const ulFilmName = this.ulFilmName.cloneNode(true);
      ulFilmName.classList.add(`episode_${film["episodeId"]}`);
      ulFilmName.textContent = film.name;
      const ulFilmEposode = this.ulEposode.cloneNode(true);
      ulFilmEposode.textContent = `Episode: ${film.episodeId}`;
      ulFilmName.appendChild(ulFilmEposode);
      const ulOpeningCrawl = this.ulEposode.cloneNode(true);
      ulOpeningCrawl.textContent = film.openingCrawl;
      ulFilmName.appendChild(ulOpeningCrawl);
      this.ulFilms.appendChild(ulFilmName);
    }
    this.film.appendChild(this.ulFilms);
  }

  renderCharacters() {
    const ul = document.createElement("ul");
    let ulCharacters = document.createElement("ul");
    for (const film of this.films) {
      let li = document.getElementsByClassName(
        `episode_${film["episodeId"]}`
      )[0];
      for (const characterUrl of film["characters"]) {
        const dataPromise = this.doGetFetchRequest(characterUrl);
        dataPromise.then((response) => {
          ulCharacters = ul.cloneNode(true);
          ulCharacters.classList.add("film__characters-list");
          ulCharacters.innerHTML = "Character name:";
          const liCharacter = document.createElement("li");
          liCharacter.classList.add("character");
          liCharacter.innerHTML = response.name;
          ulCharacters.appendChild(liCharacter);
          li.appendChild(ulCharacters);
        });
      }
    }
  }

  async doGetFetchRequest(url) {
    return (
      await fetch(url, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      })
    ).json();
  }
}

const poster = new Poster(FILMS_URL);
poster.render();
