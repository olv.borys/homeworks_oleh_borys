"use strict";


function show_hide_password(target, button){
    const input = document.getElementById(target);
    if (input.getAttribute('type') === 'password') {
    button.classList.add('view');
    input.setAttribute('type', 'text');
    } else {
    button.classList.remove('view');
    input.setAttribute('type', 'password');
    }
    return false;
  }

  function validate (event) {
      const x = document.getElementsByName ('password') [0].value,
      y = document.getElementsByName('retype_password')[0].value;
      if (x.trim() == "" || y.trim() == ""){
          alert("Поля вводу пусті!");
          return;
      }

      if (x === y) {
        document.getElementById('result').style.display="none";
        alert ("Ласкаво просимо!");
        return true;
      } else {
        event.preventDefault ();
        document.getElementById('result').style.display="block";
        // let textNode = document.createTextNode('Паролі не співпадають');
        // document.getElementById('form_wrap').appendChild(textNode);
      }
    return false;
 }
