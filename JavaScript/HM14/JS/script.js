"use strict"

//slowScrollAnchor
const allAnchor = document.querySelectorAll('.anch')
function scrollToAnchor(aid){
    let aTag = $("a[name='"+ aid.id +"']");
    $('html,body').animate({scrollTop: aTag.offset().top},3000);
}
 allAnchor.forEach(el=> el.addEventListener('click',()=>scrollToAnchor(el)))




//hide/show btn
$(document).ready(function(){
  
    $("#toggle-btn").click(function(){
       $(".wrapper-posts").css("borderBottomWidth","3px");
       $(".wrapper-posts").slideToggle(1500,function(){
          $(".wrapper-posts").css("borderBottomWidth","1px");
      });
 });
 
 });


//  //scroll btn
 (function(jq) {
    jq.autoScroll = function(ops) {
      ops = ops || {};
      ops.styleClass = ops.styleClass || 'button-up';
      const t = jq('<div class="'+ops.styleClass+'"></div>'),
     d = jq(ops.target || document);
     jq(ops.container || 'body').append(t);
   
    t.css({
      opacity: 0,
      bottom: 20,
      position: 'fixed',
      right: -10
      
   }).on('click', function() {
       jq('html,body').animate({
          scrollTop: 0
     }, ops.scrollDuration || 1000);
   });
   
    d.scroll(function() {
      const sv = d.scrollTop();
      if (sv < 300) {
        t.clearQueue().fadeOut(ops.hideDuration || 150);
       return;
    }
    let tosv = sv-$(window).height()
    t.css('display', '').clearQueue().animate({
     opacity: 0.8
     }, ops.showDuration || 400);
    });
    };
  })(jQuery);
   
  $(document).ready(function(){
   $.autoScroll({
   scrollDuration: 400, 
   showDuration: 400, 
   hideDuration: 200
   });
  });

