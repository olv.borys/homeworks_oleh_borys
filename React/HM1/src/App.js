import React from "react";
import "./App.css";
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import "./components/Button/Button.scss";
import "./components/Modal/Modal.scss";

class App extends React.Component {
  state = {
    modal: null,
  };

  openModal = (modal) => {
    this.setState({ modal });
  };

  closeModal = () => {
    this.setState({ modal: null });
  };

  render() {
    return (
      <div className="App">
        <Button
          backgroundColor="red"
          text="Open first modal"
          onClick={() =>
            this.openModal(
              <Modal
                className={"modal_body"}
                header="Do you want to delete this file?"
                closeButton="true"
                text=" Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"
                closeModal={this.closeModal}
                actions={
                  <div className={"button-container"}>
                    <Button
                      className="modal_btn"
                      text="Ok"
                      onClick={this.closeModal}
                    />
                    <Button className={"modal_btn"} text="Cancel" />
                  </div>
                }
              />
            )
          }
        />
        <Button
          backgroundColor="green"
          text="Open second modal"
          onClick={() =>
            this.openModal(
              <Modal
                header="Window Network Diagnostic"
                closeButton="true"
                closeModal={this.closeModal}
                text="The program collects information about computer hardware. Do you want to join to the Window Network Diagnostic?"
                actions={
                  <div className={"button-container"}>
                    <Button
                      className={"modal_btn"}
                      text="Yes"
                      onClick={this.closeModal}
                    />
                    <Button className={"modal_btn"} text="No" />
                  </div>
                }
              />
            )
          }
        />
        {this.state.modal}
      </div>
    );
  }
}

export default App;
