"use strict";

const btnStop = document.createElement('button');
const div = document.querySelector('.images-wrapper');
btnStop.classList.add('btn-stop');
btnStop.textContent = "Stop";

const btnResume = document.createElement('button');
btnResume.classList.add('btn-resume');
btnResume.textContent = "Resume";


const getImages = [...document.querySelectorAll('.image-to-show')];


setTimeout(()=> {
    div.after(btnStop);
    btnStop.after(btnResume);
}, 4000 );


let i = 0;
function viewImages (){
    if (i <= getImages.length && getImages[i + 1] !== undefined) {
        getImages[i].classList.add('hide-image');
        getImages[i + 1].classList.remove('hide-image');
        i++;
    } else if(getImages[i + 1] === undefined) {
        getImages[i].classList.add('hide-image');
        i = 0;
        getImages[i].classList.remove('hide-image');
    }
}


document.addEventListener("click", function clickHandler(e){ 
   console.log(e.target);
    if ( e.target.classList.contains('btn-stop')) { 
     clearInterval(interval);
     
    }else if ( e.target.classList.contains('btn-resume')) { 
        interval = setInterval(viewImages, 3000);
    }
});


let interval = setInterval(viewImages, 3000);

