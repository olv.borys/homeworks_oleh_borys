const browserSync = require("browser-sync").create();

function serv() {
   browserSync.init({
      server: {
         baseDir: "./",
      },
    
   });
}

exports.serv = serv;
exports.bs = browserSync;


