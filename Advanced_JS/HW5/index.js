"use strict";

const root = document.getElementById("root");
class Card {
  constructor(id, title, description, name, surname, email = "not resolved") {
    this.id = id + 1;
    this.title = title;
    this.description = description;
    this.name = name;
    this.surname = surname;
    this.email = email;
  }
  render() {
    if (this.email === "not resolved") {
      const post = document.createElement("div");
      post.classList.add("post");
      post.setAttribute("id", this.id);
      const headingWrapper = document.createElement("div");
      headingWrapper.classList.add("heading-wrapper");
      const author = document.createElement("h2");
      author.classList.add("author");
      author.textContent = this.name + " " + this.surname;
      post.append(headingWrapper);
      headingWrapper.append(author);
      const headin = document.createElement("p");
      headin.classList.add("post-heading-name");
      headin.textContent = this.title;
      const content = document.createElement("p");
      content.classList.add("post-description");
      content.textContent = this.description;
      headingWrapper.append(headin);
      headingWrapper.after(content);
      root.prepend(post);
    } else {
      const post = `<div class="post" id="${this.id}">
      <div class="content-wrapper">
      <div class="heading-wrapper">
        <h2 class="author">${this.name} ${this.surname}</h2>
        <button id="delete${this.id}" onclick="deletePost()">X</button>
        <h2 class="email">${this.email.toLowerCase()}</h2>
        <br>
        <p class="post-heading-name">${this.title}</p>
        </div>
        <p class="post-description">${this.description}</p>
        <img src="https://images.unian.net/photos/2016_06/1464871894-4649.png" width="489" height="220" alt="" class="post-image" />
        <div class="social-wrapper">
          <div class="comments-wrapper"><a href="" class="comments-link"><svg viewBox="0 0 24 24" aria-hidden="true"
          class="r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi" id="comment-icon">
          <g>
            <path
              d="M14.046 2.242l-4.148-.01h-.002c-4.374 0-7.8 3.427-7.8 7.802 0 4.098 3.186 7.206 7.465 7.37v3.828c0 .108.044.286.12.403.142.225.384.347.632.347.138 0 .277-.038.402-.118.264-.168 6.473-4.14 8.088-5.506 1.902-1.61 3.04-3.97 3.043-6.312v-.017c-.006-4.367-3.43-7.787-7.8-7.788zm3.787 12.972c-1.134.96-4.862 3.405-6.772 4.643V16.67c0-.414-.335-.75-.75-.75h-.396c-3.66 0-6.318-2.476-6.318-5.886 0-3.534 2.768-6.302 6.3-6.302l4.147.01h.002c3.532 0 6.3 2.766 6.302 6.296-.003 1.91-.942 3.844-2.514 5.176z">
            </path>
          </g>
        </svg>
        </a>
        </div>
          <div class="repost-wrapper"><a href="" class="repost-link"><svg viewBox="0 0 24 24" aria-hidden="true" class="r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi" id="repost-icon"><g><path d="M23.77 15.67c-.292-.293-.767-.293-1.06 0l-2.22 2.22V7.65c0-2.068-1.683-3.75-3.75-3.75h-5.85c-.414 0-.75.336-.75.75s.336.75.75.75h5.85c1.24 0 2.25 1.01 2.25 2.25v10.24l-2.22-2.22c-.293-.293-.768-.293-1.06 0s-.294.768 0 1.06l3.5 3.5c.145.147.337.22.53.22s.383-.072.53-.22l3.5-3.5c.294-.292.294-.767 0-1.06zm-10.66 3.28H7.26c-1.24 0-2.25-1.01-2.25-2.25V6.46l2.22 2.22c.148.147.34.22.532.22s.384-.073.53-.22c.293-.293.293-.768 0-1.06l-3.5-3.5c-.293-.294-.768-.294-1.06 0l-3.5 3.5c-.294.292-.294.767 0 1.06s.767.293 1.06 0l2.22-2.22V16.7c0 2.068 1.683 3.75 3.75 3.75h5.85c.414 0 .75-.336.75-.75s-.337-.75-.75-.75z"></path></g></svg></a></div>
          <div class="like-wrapper"><a href="" class="like-link">
          <svg viewBox="0 0 24 24" aria-hidden="true" class="r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi" id="like-icon"><g><path d="M12 21.638h-.014C9.403 21.59 1.95 14.856 1.95 8.478c0-3.064 2.525-5.754 5.403-5.754 2.29 0 3.83 1.58 4.646 2.73.814-1.148 2.354-2.73 4.645-2.73 2.88 0 5.404 2.69 5.404 5.755 0 6.376-7.454 13.11-10.037 13.157H12zM7.354 4.225c-2.08 0-3.903 1.988-3.903 4.255 0 5.74 7.034 11.596 8.55 11.658 1.518-.062 8.55-5.917 8.55-11.658 0-2.267-1.823-4.255-3.903-4.255-2.528 0-3.94 2.936-3.952 2.965-.23.562-1.156.562-1.387 0-.014-.03-1.425-2.965-3.954-2.965z"></path></g></svg></a></div>
          <img src="" alt="" class="share" />
        </div>
      </div>
    </div>`;
      root.innerHTML += post;
    }
  }
}
async function loadContent() {
  let response = await axios.get("https://ajax.test-danit.com/api/json/posts");
  let users = await axios.get("https://ajax.test-danit.com/api/json/users");
  document.body.querySelector(".posts-section > .loader").remove();
  for (let index = 0; index < response.data.length; index++) {
    const postEl = response.data[index];
    const randomUserIndex = Math.floor(Math.random() * users.data.length);
    const post = new Card(
      index,
      postEl.title,
      postEl.body,
      users.data[randomUserIndex].name.split(" ")[0],
      users.data[randomUserIndex].name.split(" ")[1],
      users.data[randomUserIndex].email
    );
    post.render();
  }
}
loadContent();

async function deletePost() {
  const button = event.target;
  const id = button.getAttribute("id").match(/(\d+)/)[0];
  const post = document.getElementById(id);
  let del = await axios.delete(
    `https://ajax.test-danit.com/api/json/posts/${id}`
  );
  if (del.status === 200) {
    post.classList.add("delete");
  }
  if (post.classList.contains("delete") === true) {
    setTimeout(() => post.remove(), 880);
  }
}
const btnCreate = document.querySelector(".create-post");
const form = document.querySelector(".post-form");
console.dir(form);

btnCreate.addEventListener("click", (evt) => {
  evt.preventDefault();
  form.classList.remove("hidden");
});
form[3].addEventListener("click", async (e) => {
  e.preventDefault();
  let response = await axios({
    method: "post",
    url: "https://ajax.test-danit.com/api/json/posts",
    data: {
      userId: 1,
      name: "Fred",
      fullName: "Flintstone",
      postTitle: form[0].value,
      postContent: form[1].value,
    },
  });
  const post = new Card(
    response.data.id,
    response.data.postTitle,
    response.data.postContent,
    response.data.name,
    response.data.fullName,
    "not resolved"
  );
  post.render();
  form.classList.add("hidden");
});
form[2].addEventListener("click", (evt) => {
  evt.preventDefault();
  form[1].value = "";
  form[0].value = "";
  form.classList.add("hidden");
});
