"use strict" 

   
function burgerMenu(selector) {
    let menu = $(selector);
    let button = menu.find('.burger-menu_button', '.burger-menu_lines');
    let links = menu.find('.nav__menu-link');
   
    button.on('click', (e) => {
      e.preventDefault();
      toggleMenu();
    });
    
    // links.on('click', () => toggleMenu());
    
    function toggleMenu(){
      menu.toggleClass('burger-menu_active');
    }
   
  }
  
  burgerMenu('.burger-menu');


 