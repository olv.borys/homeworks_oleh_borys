"use strict";

let numFirst = +prompt("Введіть перше число");
let numSecond = +prompt("Введіть друге число");
let operator = prompt("Введіть знак операції");

function calcResult(numFirst, numSecond, operator) {
  switch (operator) {
    case '+':
      return numFirst + numSecond;
    case '-':
      return numFirst - numSecond;
    case '*':
      return numFirst * numSecond;
    case '/':
      return numFirst / numSecond;
  }
}

console.log(calcResult(numFirst, numSecond, operator));

