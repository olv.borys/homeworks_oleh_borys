"use strict";

function createNewUser (){
  let fn = prompt ("Введіть ваше ім'я");
  let ls = prompt ("Введіть ваше прізвище");
  return { 
    firstName:fn, 
    lastName:ls,
    getLogin: function (){
      return this.firstName.toLowerCase ()[0] + this.lastName.toLowerCase();
    }, 

    getFirstName: function (){
      return this.firstName;
    },
    setFirstName: function (newFirstName) {
      this.firstName = newFirstName;
    }, 
    getLastName: function (){
      return this.lastName;
    },
    setLastName: function (newLastName) {
      this.lastName = newLastName;
    },
  }
};

const user = createNewUser();
console.log(user.getLogin());

