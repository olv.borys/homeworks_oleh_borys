const { src, dest } = require("gulp");
const cleanDist = require('gulp-clean');
const { bs } = require("./serv.js");
const cleanCSS = require('gulp-clean-css');



function cleaner() {
    return src("./dist/", {allowEmpty: true})
    .pipe(cleanDist())
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(bs.stream())

}


exports.cleaner = cleaner;


