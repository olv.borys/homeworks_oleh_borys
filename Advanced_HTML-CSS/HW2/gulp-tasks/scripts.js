const { src, dest } = require("gulp");
const { bs } = require("./serv.js");
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const sourcemaps = require("gulp-sourcemaps");

function scripts() {
   return src("./src/scripts/**/*.js")
      .pipe(sourcemaps.init())
      .pipe(concat('scripts.min.js'))
      .pipe(uglify())
      .pipe(sourcemaps.write())
      .pipe(dest("./dist/js"))
      .pipe(bs.stream());
}

exports.scripts = scripts;
