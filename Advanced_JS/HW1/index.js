"use strict";

class Employee {
    constructor (name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    };

    set name(value) {
         this._name = value;
    }
    get name (){
        return this.name
    }
    set age(value) {
         this._age = value;
    }
    get age (){
        return this.age;
    }
    set salary(value) {
        this._salary = value;
    }
    get salary (){
        return this.salary;
    }

}


class Programmer extends Employee {
    constructor (name, age, salary, lang)  {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary (){
        return this._salary * 3 ;
    }
}

const oleh = new Programmer("Oleh", 26,  10000, "ua" );



    

   