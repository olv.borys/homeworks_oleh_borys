"use strict";

function createNewUser () {

  let fn = prompt ("Введіть ваше ім'я");
  let ls = prompt ("Введіть ваше прізвище");
  let br = prompt ("Введіть дату народження");
  let dateParts = String(br).split(".");
 
  return { 

    firstName:fn, 
    lastName:ls,
    birthDay : new Date (dateParts[2], dateParts[1] - 1, dateParts[0]),

    getLogin: function (){
      return this.firstName.toLowerCase ()[0] + this.lastName.toLowerCase();
    }, 

    getFirstName: function (){
      return this.firstName;
    },
    setFirstName: function (newFirstName) {
      this.firstName = newFirstName;
    }, 
    getLastName: function (){
      return this.lastName;
    },
    setLastName: function (newLastName) {
      this.lastName = newLastName;
    },

    getAge: function () {
      let otherDate = new Date();
      let years = (otherDate.getFullYear() - this.birthDay.getFullYear());
      
      if (otherDate.getMonth () < this.birthDay.getMonth () || 
      otherDate.getMonth() === this.birthDay.getMonth() && 
      otherDate.getDate() < this.birthDay.getDate ()) {
        years --;
      }
      return years;
    },

    getPassword: function () {
    return this.firstName.toUpperCase ()[0] + this.lastName.toLowerCase() + this.birthDay.getFullYear();
    }

  }

};

const user = createNewUser();

console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());






