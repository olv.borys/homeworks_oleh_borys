import React from "react";
import {Route, Switch} from "react-router-dom";
import Main from "../Main/Main";
import Favorites from "../Favorites/Favorites";
import Cart from "../Cart/Cart";
import PropTypes from 'prop-types';

const AppRoutes = (props) => {

    const {onClick} = props;

    return (
        <Switch>
            <Route exact path="/" component={Main}>
                <Main onClick={onClick}/>
            </Route>
            <Route exact path="/Favorites" component={Favorites}/>
            <Route exact path="/Cart" component={Cart}/>
        </Switch>
    );
};

export default AppRoutes;


AppRoutes.propTypes ={
    onClick:PropTypes.func.isRequired
}