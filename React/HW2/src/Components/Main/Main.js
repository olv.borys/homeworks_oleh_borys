import React, {Component} from 'react';
import "./Main.scss"
import ProductItem from "../ProductItem/ProductItem";

class Main extends Component {

    constructor(props) {
        super(props);
    }

    state = {
        items: []
    }

    async componentDidMount() {
        await fetch("./productsStore.json")
            .then(r => r.json())
            .then(data => {
                this.setState({
                    items: data
                })
            })
    }


    render() {

        const {items} = this.state;
        const products = items.map((el, index) =>
            <ProductItem
                name={el.name}
                price={parseFloat(el.price)}
                url={el.url}
                article={el.article}
                color={el.color}
                onClick={this.props.onClick}
                key={index}
            />);

        return (
            <>
                <div className={"main"}>
                    {products}
                </div>
            </>
        );
    }
}

export default Main;
