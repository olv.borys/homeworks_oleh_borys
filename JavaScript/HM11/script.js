"use strict";

const btn = document.getElementsByClassName('btn');
window.addEventListener('keydown', changeColor);

function changeColor (event) {
  for (let i = 0; i < btn.length; i++){ 
    const currentItem = btn.item(i);
    if (currentItem.getAttribute("name") === event.code) {
      currentItem.classList.add('active');
    } else {
      currentItem.classList.remove('active');    
    }
  }
};





