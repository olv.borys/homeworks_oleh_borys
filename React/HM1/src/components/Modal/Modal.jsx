import React from "react";
import "./Modal.scss";

class Modal extends React.Component {
  render() {
    const { header, closeButton, text, actions, closeModal } = this.props;
    return (
      <section className="modal" onClick={closeModal}>
        <div className="modal_container" onClick={(e) => e.stopPropagation()}>
          <header className={"modal_header"}>
            {header}
            {closeButton && (
              <input
                type="button"
                className={"close_btn"}
                onClick={closeModal}
              />
            )}
          </header>
          <main className="modal_body">{text}</main>
          <footer className="modal__footer">{actions}</footer>
        </div>
      </section>
    );
  }
}

export default Modal;
